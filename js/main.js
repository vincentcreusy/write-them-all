angular.module('myApp', [])

.factory('factoryChampions', ($http, $q) => {
  var factory = {
    champions : {},

    getChampions : () => {
      var deferred = $q.defer()

      $http.get('json/champions.json')
      .then((data) => {
        factory.champions = angular.fromJson(data.data)
        factory.champions.url = undefined
        deferred.resolve(factory.champions)
      }, (data) => {
        deferred.reject(data)
      })

      return deferred.promise
    }
  }

  return factory
})

.controller('ctrlChampions', ($scope, $interval, factoryChampions) => {
  $scope.data = ""
  $scope.timer = 1200
  $scope.foundChampions = 0
  $scope.gameLaunched = false
  $scope.isDisabled = true
  $scope.minutes = 20;
  $scope.seconds = "00";

  $scope.champions = factoryChampions.getChampions().then((champions) => {
    $scope.champions = champions
    $scope.numberOfChamps = champions.length
  })

  var counter

  $scope.decrementTime = () => {
    $scope.timer--

    var minutes = parseInt($scope.timer / 60)
    console.log(minutes)

    $scope.minutes = (minutes < 10) ? "0" + minutes : minutes
    var seconds = $scope.timer - $scope.minutes * 60
    $scope.seconds = seconds < 10 ? "0" + ((seconds > 0) ? seconds : 0) : seconds

    if($scope.timer <= 0) {
      stop()
      $scope.timer = 0
    }
  }

  var stopTimer = () => {
    $interval.cancel(counter)
  }

  $scope.launch = () => {
    $scope.isDisabled = ""
    $scope.timer = 1200
    $scope.foundChampions = 0
    stopTimer()
    counter = $interval($scope.decrementTime, 1000)

    $scope.champions.forEach((element) => {
      element.url = " "
    })
  }

  var wink = (data, myFriendName, hisChamp) => {
    if(data === myFriendName) {
      $scope.champions.forEach((element) => {
        if(element.name === hisChamp && element.url === (undefined || " ")) {
          showImage(element, true)
          $scope.data = ""
        }
      })
    }
  }

  $scope.verifyInput = () => {
    var data = angular.lowercase($scope.data)

    $scope.champions.forEach((element) => {
      if(data === element.name && element.url === (undefined || " ")) {
        showImage(element)
        $scope.data = ""
        $scope.foundChampions++
      }
    })

    wink(data, "sylvain", "sivir")
    wink(data, "vincent", "akali")
    wink(data, "arnaud", "kalista")
    wink(data, "meir", "jhin")
    wink(data, "satan", "teemo")
    wink(data, "cancer", "yasuo")
    wink(data, "remi", "thresh")
  }

  var showImage = (champion, isTimerOn, opacity = 1) => {
    champion.url = "img/" + champion.name + ".png"
    champion.opacity = opacity;
    if(isTimerOn) {
      $scope.foundChampions++
    }
  }

  $scope.giveUp = () => {
    stop()

    showEndMessage("perdu")
  }

  var stop = () => {
    $scope.champions.forEach((element) => {
      if(element.url !== "img/" + element.name + ".png") {
        showImage(element, false, 0.5)
      }
    })
    $scope.isDisabled = true
    stopTimer()
  }

  var showEndMessage = (message) => {
    $scope.endMessage = "Vous avez " + message + "."
  }

  $scope.addAChamp = () => {
    $scope.arrayCopy = []

    $scope.champions.forEach((element) => {
      if(element.url === (undefined || " ")) {
        $scope.arrayCopy.push(element)
      }
    })

    var champToAdd = $scope.arrayCopy[Math.floor(Math.random() * $scope.arrayCopy.length)]

    if(champToAdd != undefined) {
      $scope.data = champToAdd.name
      $scope.verifyInput()
      for(var i = 0; i < 60; i++) {
        $scope.decrementTime()
      }
    }
  }
})
